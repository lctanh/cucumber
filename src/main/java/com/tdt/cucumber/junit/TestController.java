package com.tdt.cucumber.junit;


import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @RequestMapping(method = {RequestMethod.GET}, value = {"/hello"})
    public String sayHello(HttpServletResponse response) {
        return "Hello";
    }

    @RequestMapping(method = {RequestMethod.POST}, value = {"/test"})
    public String sayHelloPost(HttpServletResponse response) {
        return "Hello";
    }

}
