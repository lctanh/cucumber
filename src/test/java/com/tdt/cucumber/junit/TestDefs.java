package com.tdt.cucumber.junit;

import com.tdt.cucumber.common.SpringIntegrationTest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class TestDefs extends SpringIntegrationTest {
    private static final Logger logger = LoggerFactory.getLogger(TestDefs.class);

    @When("^the client calls /test$")
    public void callTest() throws Throwable {
        logger.info("callTest() - IN");
        executePost("http://localhost:8080/test");
        logger.info("callTest() - OUT");
    }

    @Given("^the client calls /hello$")
    public void callHello() throws Throwable {
        logger.info("callHello() - IN");
        executeGet("http://localhost:8080/hello");
        logger.info("callHello() - OUT");
    }

}
