package com.tdt.cucumber.junit;


import com.tdt.cucumber.common.SpringIntegrationTest;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class VersionDefs extends SpringIntegrationTest {
    private static final Logger logger = LoggerFactory.getLogger(VersionDefs.class);

    @When("^the client calls /version$")
    public void sendGetVersion() throws Throwable {
        logger.info("sendGetVersion() - IN");
        executeGet("http://localhost:8080/version");
        logger.info("sendGetVersion() - OUT");
    }

    @Then("^the client receives status code of (\\d+)$")
    public void receiveStatusCode(int statusCode) throws Throwable {
        logger.info("receiveStatusCode() - IN");
        HttpStatus currentStatusCode = latestResponse.getTheResponse().getStatusCode();
        logger.info("Current status code = {}", currentStatusCode);
        assertThat("status code is incorrect : " + latestResponse.getBody(), currentStatusCode.value(), is(statusCode));
        logger.info("receiveStatusCode() - OUT");
    }

    @And("^the client receives server version (.+)$")
    public void receiveVersionBody(String version) throws Throwable {
        logger.info("receiveVersionBody() - IN");
        String serverVersion = latestResponse.getBody();
        logger.info("Server version: {}", serverVersion);
        assertThat(serverVersion, is(version));
        logger.info("receiveVersionBody() - OUT");
    }

}
