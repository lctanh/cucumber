package com.tdt.cucumber.common;

public class Constant {
    public static final String HEADER_ACCEPT = "Accept";

    public static final String HEADER_TYPE = "application/json";
}
