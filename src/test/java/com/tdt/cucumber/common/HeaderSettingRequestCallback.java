package com.tdt.cucumber.common;


import org.springframework.http.HttpHeaders;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.web.client.RequestCallback;

import java.io.IOException;
import java.util.Map;

public class HeaderSettingRequestCallback implements RequestCallback{
    private  Map<String, String> requestHeaders;

    private String body;

    public HeaderSettingRequestCallback(Map<String, String> headers) {
        this.requestHeaders = headers;
    }

    public void setBody(String postBody) {
        this.body = postBody;
    }

    @Override
    public void doWithRequest(ClientHttpRequest request) throws IOException {
        HttpHeaders clientHeaders = request.getHeaders();
        for (Map.Entry<String, String> entry : requestHeaders.entrySet()) {
            clientHeaders.add(entry.getKey(), entry.getValue());
        }
        if (null != body) {
            request.getBody().write(body.getBytes());
        }
    }
}
